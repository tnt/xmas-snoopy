Xmas Snoopy
===========

This repository contains all the source code and design files for a
Snoopy themed LED matrix chrismas decoration.


License
-------

A lots of the files will have appropriate SPDX license identifiers in their
headers.

In general for this project :

  * Documents / Data / Images are under `CC-BY-SA 4.0`
  * Hardware and Gateware designs are under `CERN-OHL-P-2.0`
  * Software is under `MIT`, `GPL-3.0-or-later` or `LGPL-3.0-or-later`

See the `doc/licenses` subdirectory for the full text of all those licenses.

Also note that some files are included here from other projects, either copied
here or included using git submodules and those will have their own licensing.
