Electrical Engineering Change Notes
===================================

Revision 1.0
------------

### Description

First revision


### Manufactured

Boards ordered from Aisler on January 6th 2023.
Made directly from kicad files in r1.0 directory.

Two of the boards were assembled and tested.


### Issues noticed

* Leaks from the 5V into the FPGA IOs cause two issues:
    * The 3V3 rail rises triggering a "turn-on" of the soft-off circuit
    * The FPGA goes into a weird mode where its IOs are not properly
      configured because it sees voltage on its IO before being properly
      powered.

  Theses leaks come from two path:
    * The `CHG_FPGA_n` line through the LED
    * The `SENSE` line from the voltage divider

* Voltages on the USB lines (most importantly through the pull-up) end
  up leaking on the 5V rail through the ESD protection diode, which in
  turn causes the `SENSE` line to be triggered ...

  I think ideally the pull-up from the FPGA should actually go through
  and external NMOS that enables pull-up from 5V rail.

* The 5V rail doesn't drain when disconnected.

* The soft-off circuit wouldn't actually turn-off.

* When the battery is not connected, the system can't be operated. The
  charging circuits charges C16 but when something starts drawing current,
  it stops and takes to much time to "start again" and C16 depletes.
  That causes the FPGA to misconfigure / misbehave.

* Soldering the battery to raw pads isn't practical, it's useful to be able
  to disconnect it. So using a proper connector would be better.

* The sound output doesn't really work. The speaker have too low impedance
  and so they can't be driven from the IO directly at any reasonable volume.
  That feature was abandonned.


### Reworks

To make them fit for purposes, some changes were applied to both assembled
boards.

* Replace R18 with a diode to fix the 5V leak. The `CHG_FPGA_n` can be
  internally pulled up and will be dropped to about 0.6v ( < Vil ) by
  the charge chip when charging.

* Replace the R14/R17 divider by a N-mosfet to fix the 5V leak.
  Wired as  G = `5V`, S = `GND`, D = `SENSE`.
  When 5V is present, it can charge the mosfet gate and force `SENSE` to
  ground. And then also use internal pull-up for that line. The 5V rail
  may need an active discharge resistor to make sure the gate discharges
  when USB is disconnected. Not tested.

* Cut the 5V rail from the ESD diode to prevent voltages on the data line
  to leak to an un-powered 5V rail.

* Added a 1M pulldown on 5V rail so it discharges.

* Replaced C14 with a 2.2uF cap in parallel with a 100k resistor.
  This makes the `PWR_EN` line only raise to 1.6V which is enough to enable
  the regulator but makes it easier to discharge without it re-triggering.
  Note that this is still a bit dodgy and in further revisions, the whole
  circuit should be overhauled.

* Soldered a 47uF (ZRB18AR60J476ME01L) in parallel to C16.
  Not perfect and seems flash programming can fail sometimes, but works
  most of the time for quick battery-less tests.

* Used Molex PicoBlade 53047-0210 as connector for the battery.
  It fits in the footprint that was on board and in the space available.
