Electrical Engineering Change Notes
===================================

Revision 1.0
------------

### Description

First revision


### Manufactured

Boards ordered from PCBWay on December 26th 2023.
Made from gerbers in r1.0/fab directory.

Single board assembled for testing
