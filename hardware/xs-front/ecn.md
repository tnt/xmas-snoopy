Electrical Engineering Change Notes
===================================

Revision 1.0
------------

### Description

First revision


### Manufactured

Boards ordered from PCBWay on December 26th 2023.
Made from gerbers in r1.0/fab directory.

Note that it's important to check with the fab that
they do not crop/extend/modify the silk and mask layers
or this will ruin the graphics.

Two of the boards were assembled and tested.


Some boards were also made by elecrow but they screwed
with the mask/silk layers, damaging the "art". Those are
not usable.


### Issues noticed

* No good attachement points. Both the "solder tab" and the
  small hole end up covered by the controller.

* No good method to securely attach the controller.
  Double sided tape will have to do.
