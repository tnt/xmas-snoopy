/*
 * top_tb.v
 *
 * vim: ts=4 sw=4
 *
 * Copyright (C) 2023  Sylvain Munaut <tnt@246tNt.com>
 * SPDX-License-Identifier: CERN-OHL-P-2.0
 */

`default_nettype none

module top_tb;

	// Signals
	// -------

	wire [3:0] spi_io;
	wire       spi_cs_n;
	wire       spi_clk;

	wire usb_dp;
	wire usb_dn;
	wire usb_pu;


	// Setup recording
	// ---------------

	initial begin
		$dumpfile("top_tb.vcd");
		$dumpvars(0,top_tb);
		# 2000000 $finish;
	end


	// DUT
	// ---

	top dut_I (
		.spi_io   (spi_io),
		.spi_clk  (spi_clk),
		.spi_cs_n (spi_cs_n),
		.usb_dp   (usb_dp),
		.usb_dn   (usb_dn),
		.usb_pu   (usb_pu)
	);


	// Support
	// -------

	pullup(usb_dp);
	pullup(usb_dn);

	spiflash flash_I (
		.csb (spi_cs_n),
		.clk (spi_clk),
		.io0 (spi_io[0]),
		.io1 (spi_io[1]),
		.io2 (spi_io[2]),
		.io3 (spi_io[3])
	);

endmodule // top_tb
