/*
 * soc_usb_buf_bridge.v
 *
 * vim: ts=4 sw=4
 *
 * Copyright (C) 2023  Sylvain Munaut <tnt@246tNt.com>
 * SPDX-License-Identifier: CERN-OHL-P-2.0
 */

`default_nettype none

module soc_usb_buf_bridge (
	// Wishbone (from SoC)
	input  wire [15:0] wb_addr,
	output reg  [31:0] wb_rdata,
	input  wire [31:0] wb_wdata,
	input  wire [ 3:0] wb_wmsk,
	input  wire        wb_we,
	input  wire        wb_cyc,
	output reg         wb_ack,

	// USB EP buffer
	output wire [ 8:0] ep_tx_addr_0,
	output wire [31:0] ep_tx_data_0,
	output wire        ep_tx_we_0,

	output wire [ 8:0] ep_rx_addr_0,
	input  wire [31:0] ep_rx_data_1,
	output wire        ep_rx_re_0,

	// Clock / Reset
	input  wire clk,
	input  wire rst
);

	// Ack
	always @(posedge clk)
		wb_ack <= wb_cyc & ~wb_ack;

	// Address
	assign ep_tx_addr_0 = wb_addr[8:0];
	assign ep_rx_addr_0 = wb_addr[8:0];

	// Read Enable
	assign ep_rx_re_0 = wb_cyc;

	// Read
	always @(*)
		if (~wb_ack)
			wb_rdata = 32'h00000000;
		else
			wb_rdata = ep_rx_data_1;

	// Write data
	assign ep_tx_data_0 = wb_wdata;

	// Write enable
	assign ep_tx_we_0 = wb_ack & wb_we;

endmodule // soc_usb_buf_bridge
