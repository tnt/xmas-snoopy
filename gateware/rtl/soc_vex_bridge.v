/*
 * soc_vex_bridge.v
 *
 * vim: ts=4 sw=4
 *
 * Copyright (C) 2020-2023  Sylvain Munaut <tnt@246tNt.com>
 * SPDX-License-Identifier: CERN-OHL-P-2.0
 */

`default_nettype none

module soc_vex_bridge #(
	parameter integer WB_N   =  8,
	parameter integer WB_DW  = 32,
	parameter integer WB_AW  = 16,
	parameter integer WB_AI  =  2
)(
	/* Vex ISimpleBus */
	input  wire        ic_valid,
	output wire        ic_ready,
	input  wire [31:0] ic_pc,
	output reg         ir_valid,
	output wire        ir_error,
	output wire [31:0] ir_inst,

	/* Vex DSimpleBus */
	input  wire        dc_valid,
	output wire        dc_ready,
	input  wire        dc_wr,
	input  wire [31:0] dc_address,
	input  wire [31:0] dc_data,
	input  wire [ 1:0] dc_size,
	output wire        dr_ready,
	output wire        dr_error,
	output wire [31:0] dr_data,

	/* BRAM */
	output wire [14:0] bram_addr,
	input  wire [31:0] bram_rdata,
	output wire [31:0] bram_wdata,
	output wire [ 3:0] bram_wmsk,
	output wire        bram_we,

	/* SPRAM */
	output wire [14:0] spram_addr,
	input  wire [31:0] spram_rdata,
	output wire [31:0] spram_wdata,
	output wire [ 3:0] spram_wmsk,
	output wire        spram_we,

	/* Wishbone buses */
	output reg  [WB_AW-1:0]        wb_addr,
	input  wire [(WB_DW*WB_N)-1:0] wb_rdata,
	output reg  [WB_DW-1:0]        wb_wdata,
	output reg  [(WB_DW/8)-1:0]    wb_wmsk,
	output reg                     wb_we,
	output reg  [WB_N-1:0]         wb_cyc,
	input  wire [WB_N-1:0]         wb_ack,

	/* Clock / Reset */
	input  wire clk,
	input  wire rst
);

	genvar i;

	// Signals
	// -------

	wire [31:0] ram_addr;
	wire [31:0] ram_rdata;
	wire [31:0] ram_wdata;
	wire [ 3:0] ram_wmsk;
	wire        ram_we;

	wire        ram_user;	// 0=IBus 1=DBus
	reg         ram_sel_r;

	wire            wb_start;
	reg             wb_busy;
	wire [WB_N-1:0] wb_match;
	reg             wb_ack_r;
	reg  [    31:0] wb_rdata_or;
	reg  [    31:0] wb_rdata_r;

	reg dr_ready_ram;


	// RAM access
	// ----------
	// BRAM  : 0x00000000 -> 0x000003ff
	// SPRAM : 0x00020000 -> 0x0003ffff

	// Commands
	assign ram_user  = dc_valid & ~dc_address[31];
	assign ram_addr  = ram_user ? dc_address : ic_pc;

	assign ram_wdata = dc_data;
	assign ram_wmsk  = ~(((1 << (1 << dc_size)) - 1) << dc_address[1:0]);
	assign ram_we    = ram_user & dc_wr;

	// Keep some info about the access
	always @(posedge clk)
		ram_sel_r  <= ram_addr[17];

	// BRAM
	assign bram_addr  = ram_addr[16:2];
	assign bram_wdata = ram_wdata;
	assign bram_wmsk  = ram_wmsk;
	assign bram_we    = ram_we & ~ram_addr[17];

	// SPRAM
	assign spram_addr  = ram_addr[16:2];
	assign spram_wdata = ram_wdata;
	assign spram_wmsk  = ram_wmsk;
	assign spram_we    = ram_we & ram_addr[17];


	// Read Mux
	assign ram_rdata = ram_sel_r ? spram_rdata : bram_rdata;


	// Wishbone
	// --------
	// wb[x] = 0x8x000000 - 0x8xffffff

	// Busy
	assign wb_start = dc_valid & dc_address[31] & ~wb_busy;

	always @(posedge clk)
		if (rst)
			wb_busy <= 1'b0;
		else
			wb_busy <= (wb_busy & ~wb_ack_r) | wb_start;

	// Register to keep value stable during bus access
		// Wishbone need values to be stable
		// But the Vex Bus won't do that unless we delay the 'dc_ready'
		// and we can't ack the command and provide response in same cycle
		// So easier to save those in the same cycle we do the decode
		// and ack directly
	always @(posedge clk)
	begin
		if (wb_start) begin
			wb_addr  <= dc_address[WB_AW+1:2];
			wb_wdata <= dc_data;
			wb_wmsk  <= ~(((1 << (1 << dc_size)) - 1) << dc_address[1:0]);
			wb_we    <= dc_wr;
		end
	end

	// Cycle
	for (i=0; i<WB_N; i=i+1)
		assign wb_match[i] = (dc_address[27:24] == i);

	always @(posedge clk)
		if (rst)
			wb_cyc <= 0;
		else
			wb_cyc <= (wb_cyc & ~wb_ack) | (wb_match & {WB_N{wb_start}});

	// Ack
	always @(posedge clk)
		wb_ack_r <= |wb_ack;

	// Read data
	always @(*)
	begin : wb_or
		integer i;
		wb_rdata_or = 0;
		for (i=0; i<WB_N; i=i+1)
			wb_rdata_or[WB_DW-1:0] = wb_rdata_or[WB_DW-1:0] | wb_rdata[WB_DW*i+:WB_DW];
	end

	always @(posedge clk)
		wb_rdata_r <= wb_rdata_or;


	// IBus
	// ----

	assign ic_ready = ~ram_user;

	always @(posedge clk)
		ir_valid <= ic_valid & ic_ready;

	assign ir_error = 1'b0;
	assign ir_inst  = ram_rdata;


	// DBus
	// ----

	assign dc_ready = (dc_valid & ~dc_address[31]) | wb_start;

	always @(posedge clk)
		dr_ready_ram <= dc_valid & ~dc_address[31] & ~dc_wr;

	assign dr_ready = dr_ready_ram | (wb_ack_r & ~wb_we);	// wb_we is still valid
	assign dr_error = 1'b0;
	assign dr_data  = dr_ready_ram ? ram_rdata : wb_rdata_r;

endmodule // soc_vex_bridge
