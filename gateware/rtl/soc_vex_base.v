/*
 * soc_vex_base.v
 *
 * vim: ts=4 sw=4
 *
 * Copyright (C) 2019-2023  Sylvain Munaut <tnt@246tNt.com>
 * SPDX-License-Identifier: CERN-OHL-P-2.0
 */

`default_nettype none

module soc_vex_base #(
	parameter integer WB_N  =  6,
	parameter integer WB_DW = 32,
	parameter integer WB_AW = 16,
	parameter integer BRAM_AW  =  8,	/* Default 1k */
	parameter integer SPRAM_AW = 14,	/* 14 => 64k, 15 => 128k */

	/* auto */
	parameter integer WB_MW = WB_DW / 8,
	parameter integer WB_RW = WB_DW * WB_N,
	parameter integer WB_AI = $clog2(WB_MW)
)(
	// Wishbone
	output wire [WB_AW-1:0] wb_addr,
	input  wire [WB_RW-1:0] wb_rdata,
	output wire [WB_DW-1:0] wb_wdata,
	output wire [WB_MW-1:0] wb_wmsk,
	output wire             wb_we,
	output wire [WB_N -1:0] wb_cyc,
	input  wire [WB_N -1:0] wb_ack,

	// Clock / Reset
	input  wire clk,
	input  wire rst
);

	// Signals
	// -------

	// Vex ISimpleBus
	wire        ic_valid;
	wire        ic_ready;
	wire [31:0] ic_pc;
	wire        ir_valid;
	wire        ir_error;
	wire [31:0] ir_inst;

	// Vex DSimpleBus
	wire        dc_valid;
	wire        dc_ready;
	wire        dc_wr;
	wire [31:0] dc_address;
	wire [31:0] dc_data;
	wire [ 1:0] dc_size;
	wire        dr_ready;
	wire        dr_error;
	wire [31:0] dr_data;

	// RAM
		// BRAM
	wire [14:0] bram_addr;
	wire [31:0] bram_rdata;
	wire [31:0] bram_wdata;
	wire [ 3:0] bram_wmsk;
	wire        bram_we;

		// SPRAM
	wire [14:0] spram_addr;
	wire [31:0] spram_rdata;
	wire [31:0] spram_wdata;
	wire [ 3:0] spram_wmsk;
	wire        spram_we;


	// CPU
	// ---

	VexRiscv cpu_I (
		.iBus_cmd_valid           (ic_valid),
		.iBus_cmd_ready           (ic_ready),
		.iBus_cmd_payload_pc      (ic_pc),
		.iBus_rsp_valid           (ir_valid),
		.iBus_rsp_payload_error   (ir_error),
		.iBus_rsp_payload_inst    (ir_inst),
		.dBus_cmd_valid           (dc_valid),
		.dBus_cmd_ready           (dc_ready),
		.dBus_cmd_payload_wr      (dc_wr),
		.dBus_cmd_payload_address (dc_address),
		.dBus_cmd_payload_data    (dc_data),
		.dBus_cmd_payload_size    (dc_size),
		.dBus_rsp_ready           (dr_ready),
		.dBus_rsp_error           (dr_error),
		.dBus_rsp_data            (dr_data),
		.clk                      (clk),
		.reset                    (rst)
	);


	// Bus interface
	// -------------

	soc_vex_bridge #(
		.WB_N (WB_N),
		.WB_DW(WB_DW),
		.WB_AW(WB_AW),
		.WB_AI(WB_AI)
	) vb_I (
		.ic_valid    (ic_valid),
		.ic_ready    (ic_ready),
		.ic_pc       (ic_pc),
		.ir_valid    (ir_valid),
		.ir_error    (ir_error),
		.ir_inst     (ir_inst),
		.dc_valid    (dc_valid),
		.dc_ready    (dc_ready),
		.dc_wr       (dc_wr),
		.dc_address  (dc_address),
		.dc_data     (dc_data),
		.dc_size     (dc_size),
		.dr_ready    (dr_ready),
		.dr_error    (dr_error),
		.dr_data     (dr_data),
		.bram_addr   (bram_addr),
		.bram_rdata  (bram_rdata),
		.bram_wdata  (bram_wdata),
		.bram_wmsk   (bram_wmsk),
		.bram_we     (bram_we),
		.spram_addr  (spram_addr),
		.spram_rdata (spram_rdata),
		.spram_wdata (spram_wdata),
		.spram_wmsk  (spram_wmsk),
		.spram_we    (spram_we),
		.wb_addr     (wb_addr),
		.wb_wdata    (wb_wdata),
		.wb_wmsk     (wb_wmsk),
		.wb_rdata    (wb_rdata),
		.wb_cyc      (wb_cyc),
		.wb_we       (wb_we),
		.wb_ack      (wb_ack),
		.clk         (clk),
		.rst         (rst)
	);


	// Local memory
	// ------------

	// Boot memory
	soc_bram #(
		.SIZE(1 << BRAM_AW),
		.INIT_FILE("boot.hex")
	) bram_I (
		.addr  (bram_addr[BRAM_AW-1:0]),
		.rdata (bram_rdata),
		.wdata (bram_wdata),
		.wmsk  (bram_wmsk),
		.we    (bram_we),
		.clk   (clk)
	);

	// Main memory
	soc_spram #(
		.AW(SPRAM_AW)
	) spram_I (
		.addr  (spram_addr[SPRAM_AW-1:0]),
		.rdata (spram_rdata),
		.wdata (spram_wdata),
		.wmsk  (spram_wmsk),
		.we    (spram_we),
		.clk   (clk)
	);

endmodule // soc_vex_base
