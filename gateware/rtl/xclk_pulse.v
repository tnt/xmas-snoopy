/*
 * xclk_pulse.v
 *
 * vim: ts=4 sw=4
 *
 * Helper to cross a pulse from one clock domain to another
 *
 * Copyright (C) 2023  Sylvain Munaut <tnt@246tNt.com>
 * SPDX-License-Identifier: CERN-OHL-P-2.0
 */

`default_nettype none

module xclk_pulse (
	input  wire i_stb,
	input  wire i_clk,
	output wire o_stb,
	input  wire o_clk,
	input  wire rst
);

	reg       i_toggle;
	reg [2:0] o_sync;

	always @(posedge i_clk or posedge rst)
		if (rst)
			i_toggle <= 1'b0;
		else
			i_toggle <= i_toggle ^ i_stb;

	always @(posedge o_clk or posedge rst)
		if (rst)
			o_sync <= 3'b000;
		else
			o_sync <= { o_sync[1] ^ o_sync[0], o_sync[0], i_toggle };

	assign o_stb = o_sync[2];

endmodule // xclk_pulse
