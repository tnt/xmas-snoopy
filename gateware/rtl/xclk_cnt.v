/*
 * xclk_cnt.v
 *
 * vim: ts=4 sw=4
 *
 * Helper to pass a counter value from one domain to another
 * (assuming the counter is increment/decrement/wrap only !)
 *
 * Copyright (C) 2023  Sylvain Munaut <tnt@246tNt.com>
 * SPDX-License-Identifier: CERN-OHL-P-2.0
 */

`default_nettype none

module xclk_cnt #(
	parameter integer WIDTH = 4
)(
	input  wire [WIDTH-1:0] i_val,
	input  wire i_clk,
	output wire [WIDTH-1:0] o_val,
	input  wire o_clk,
	input  wire rst
);

	// Encode to gray in source domain

	// Capture in destination domain

	// Decode from gray in destination domain

	// FIXME
	assign o_val = i_val;

endmodule // xclk_cnt
