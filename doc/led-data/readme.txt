analysis.txt
	Choice of SB_RGBA_DRV settings for those leds
	and associated scaing factor

drv-iv/
	I/V curve for the SB_RGBA_DRV
	Keithley 2400 sourcing a voltage and measuring current
	For each current setting, for each RGB0/1/2 pins and swept twice

led-iv/
	I/V curve for the leds
	Keithley 2400 sourcing current and measuring voltage across
	0-25 mA current, swept 4 times
