/*
 * usb_desc_ids.h
 *
 * Copyright (C) 2023  Sylvain Munaut <tnt@246tNt.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#define USB_INTF_DFU		0
#define USB_INTF_NUM		1
