/*
 * led_anim.h
 *
 * Copyright (C) 2023  Sylvain Munaut <tnt@246tNt.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <stdint.h>


struct led_anim_config {
	uint8_t dim;	/* 255 = Normal */
	uint8_t speed;	/*  64 = Normal */
};

typedef void (*led_anim_init_fn)  (void *state);
typedef void (*led_anim_render_fn)(void *state, uint16_t *leds, struct led_anim_config *conf, uint32_t frame);

struct led_anim {
	led_anim_init_fn   init;
	led_anim_render_fn render;
};

extern const struct led_anim * const all_anims[];
