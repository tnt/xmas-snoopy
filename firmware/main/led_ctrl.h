/*
 * led_ctrl.h
 *
 * Copyright (C) 2023  Sylvain Munaut <tnt@246tNt.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

void led_init(void);
void led_start(void);
void led_stop(void);

void led_cycle_dim(void);
void led_cycle_speed(void);
void led_cycle_anim(void);

void led_poll(bool suspend);
