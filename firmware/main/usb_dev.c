/*
 * usb_dev.c
 *
 * Copyright (C) 2023  Sylvain Munaut <tnt@246tNt.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <stdint.h>
#include <string.h>

#include <no2usb/usb.h>
#include <no2usb/usb_proto.h>

#include "i2c.h"


#define XS_DEV_I2C_REG_ACCESS		0x10
#define XS_DEV_I2C_XFER			0x1e
#define XS_DEV_I2C_PROBE		0x1f


static struct {
	int     len;
	uint8_t data[256];
} g_i2c_buf;


static bool
i2c_xfer_req_cb(struct usb_xfer *xfer)
{
	struct usb_ctrl_req *req = xfer->cb_ctx;
	g_i2c_buf.len = req->wValue & 0xff;
	return i2c_xfer(
		(req->wIndex >> 8),
		xfer->data,
		xfer->len,
		g_i2c_buf.data,
		g_i2c_buf.len
	);
}


static enum usb_fnd_resp
_usb_dev_ctrl_req_write(struct usb_ctrl_req *req, struct usb_xfer *xfer)
{
	switch (req->bRequest) {
	case XS_DEV_I2C_REG_ACCESS:
		if (!i2c_write_reg((req->wIndex >> 8), req->wIndex & 0xff, req->wValue & 0xff))
			return USB_FND_ERROR;
		break;
	case XS_DEV_I2C_XFER:
		xfer->cb_done = i2c_xfer_req_cb;
		xfer->cb_ctx  = req;
		break;
	case XS_DEV_I2C_PROBE:
		return i2c_probe(req->wIndex >> 8) ? USB_FND_SUCCESS : USB_FND_ERROR;
	default:
		return USB_FND_ERROR;
	}

	return USB_FND_SUCCESS;
}

static enum usb_fnd_resp
_usb_dev_ctrl_req_read(struct usb_ctrl_req *req, struct usb_xfer *xfer)
{
	switch (req->bRequest) {
	case XS_DEV_I2C_REG_ACCESS:
		if (!i2c_read_reg((req->wIndex >> 8), req->wIndex & 0xff, &xfer->data[0]))
			return USB_FND_ERROR;
		xfer->len = 1;
		break;
	case XS_DEV_I2C_XFER:
		xfer->data = g_i2c_buf.data;
		xfer->len  = g_i2c_buf.len;
		break;
	case XS_DEV_I2C_PROBE:
		xfer->data[0] = i2c_probe(req->wIndex >> 8);
		xfer->len = 1;
		break;
	default:
		return USB_FND_ERROR;
	}

	return USB_FND_SUCCESS;
}

static enum usb_fnd_resp
_usb_dev_ctrl_req(struct usb_ctrl_req *req, struct usb_xfer *xfer)
{
	/* Check it's a device-wide vendor request */
	if (USB_REQ_TYPE_RCPT(req) != (USB_REQ_TYPE_VENDOR | USB_REQ_RCPT_DEV))
		return USB_FND_CONTINUE;

	/* Read / Write dispatch */
	if (USB_REQ_IS_READ(req))
		return _usb_dev_ctrl_req_read(req, xfer);
	else
		return _usb_dev_ctrl_req_write(req, xfer);
}


static struct usb_fn_drv _dev_drv = {
	.ctrl_req = _usb_dev_ctrl_req,
};

void
usb_dev_init(void)
{
	usb_register_function_driver(&_dev_drv);
}
