/*
 * pmu.h
 *
 * Copyright (C) 2022  Sylvain Munaut <tnt@246tNt.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <stdbool.h>
#include <stdint.h>

void    pmu_sys_suspend(void);
void    pmu_sys_shutdown(void);
void    pmu_sys_reboot(int n);

void    pmu_usb_enable(void);
void    pmu_usb_disable(void);
void    pmu_usb_set_state(bool state);
bool    pmu_usb_state(void);

bool    pmu_is_charging(void);
bool    pmu_is_vbus_present(void);

#define BTN_2_LONG	(1 << 3)
#define BTN_2_SHORT	(1 << 2)
#define BTN_1_LONG	(1 << 1)
#define BTN_1_SHORT	(1 << 0)

uint8_t pmu_get_buttons(void);
