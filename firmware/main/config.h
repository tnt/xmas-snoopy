/*
 * config.h
 *
 * Copyright (C) 2023  Sylvain Munaut <tnt@246tNt.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#define PMU_BASE	0x80000000
#define SPI_BASE	0x81000000
#define I2C_BASE	0x82000000
#define USB_DATA_BASE	0x83000000
#define USB_CORE_BASE	0x84000000
#define LED_CTRL_BASE	0x85000000

#define SYS_CLK_FREQ	24000000
