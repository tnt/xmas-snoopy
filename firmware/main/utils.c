/*
 * utils.c
 *
 * Copyright (C) 2019-2023  Sylvain Munaut <tnt@246tNt.com>
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include <stdarg.h>
#include <stdint.h>
#include <stdbool.h>


char *
hexstr(void *d, int n, bool space)
{
	static const char * const hex = "0123456789abcdef";
	static char buf[96];
	uint8_t *p = d;
	char *s = buf;
	char c;

	while (n--) {
		c = *p++;
		*s++ = hex[c >> 4];
		*s++ = hex[c & 0xf];
		if (space)
			*s++ = ' ';
	}

	s[space?-1:0] = '\0';

	return buf;
}

uint8_t
hexval(char c)
{
	if (c >= '0' && c <= '9')
		return c - '0';
	else if (c >= 'a' && c <= 'f')
		return 10 + (c - 'a');
	else if (c >= 'A' && c <= 'F')
		return 10 + (c - 'A');
	else
		return 0;
}


uint16_t
rand16()
{
	static uint32_t rng = 1234;
        rng = (rng * 22695477) + 1;
        return rng >> 12;
}
